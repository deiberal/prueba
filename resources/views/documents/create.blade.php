@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form class="card" action="{{ route('documentos.store') }}" method="POST">
                    @csrf
                    <div class="card-header">Crear Documento
                        <a href="{{ route('home') }}" class="btn btn-dark btn-sm float-right">Volver</a>
                    </div>
                    <div class="card-body">
                        @include('documents.fields')
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" type="submit">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
@endsection
