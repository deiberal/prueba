<tr>
    <td>{{ $document->id }}</td>
    <td>{{ $document->DOC_NOMBRE }}</td>
    <td>{{ $document->DOC_CODIGO }}</td>
    <td>{{ $document->tipo()->first()->TIP_NOMBRE }}</td>
    <td>{{ $document->proceso()->first()->PRO_NOMBRE }}</td>
    <td>
        <div class="btn-group btn-group-sm">
            <a href="{{ route('documentos.edit',['id'=>$document->id]) }}" class="btn btn-info">Editar</a>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $document->id }}">
                Eliminar
            </button>
            <!-- Modal -->
            <div class="modal fade" id="deleteModal{{ $document->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel{{ $document->id }}" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="{{ route('documentos.destroy',['id'=>$document->id]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel{{ $document->id }}">Eliminar Documento - {{ $document->DOC_CODIGO }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </td>

</tr>
