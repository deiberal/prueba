@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form class="card" action="{{ route('documentos.update',['id'=>$documento->id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card-header">Editar Documento - {{ $documento->DOC_CODIGO }}
                        <a href="{{ route('home') }}" class="btn btn-dark btn-sm float-right">Volver</a>
                    </div>
                    <div class="card-body">
                        @include('documents.fields', ['documento' => $documento])
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" type="submit">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
@endsection
