<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="DOC_NOMBRE">Nombre</label>
            <input type="text" name="DOC_NOMBRE" id="DOC_NOMBRE" class="form-control"
                   value="{{ $documento->DOC_NOMBRE ?? '' }}"
                   placeholder="Nombre del documento">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="DOC_TIPO">Tipo</label>
            <select name="DOC_TIPO" id="DOC_TIPO" class="form-control">
                <option value="">Seleccione</option>
                @foreach($tipos as $tipo)
                    <option
                        value="{{ $tipo->id }}" {{ ($documento->DOC_ID_TIPO??'') == $tipo->id?'selected':'' }}>{{ $tipo->TIP_NOMBRE }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <label for="DOC_PROCESO">Proceso</label>
        <select name="DOC_PROCESO" id="DOC_PROCESO" class="form-control">
            <option value="">Seleccione</option>
            @foreach($procesos as $proceso)
                <option
                    value="{{ $proceso->id }}" {{ ($documento->DOC_ID_PROCESO??'') == $proceso->id?'selected':'' }}>{{ $proceso->PRO_NOMBRE }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <label for="DOC_CONTENIDO">Contenido</label>
        <textarea class="form-control" name="DOC_CONTENIDO" id="DOC_CONTENIDO"
                  placeholder="Contenido">{{ $documento->DOC_CONTENIDO ?? '' }}</textarea>
    </div>
</div>
