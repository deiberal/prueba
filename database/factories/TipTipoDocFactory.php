<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\TipTipoDoc;
use Faker\Generator as Faker;

$factory->define(TipTipoDoc::class, function (Faker $faker) {
    $word = strtoupper($faker->word);
    //Sacar las 3 primeras letras
    $prefijo = substr($word, 0, 3);
    return [
        'TIP_NOMBRE' => $word,
        'TIP_PREFIJO' => $prefijo,
    ];
});
