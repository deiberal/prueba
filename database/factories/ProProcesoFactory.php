<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ProProceso;
use Faker\Generator as Faker;

$factory->define(ProProceso::class, function (Faker $faker) {
    $word = strtoupper($faker->word);
    //Sacar las 3 primeras letras
    $prefijo = substr($word, 0, 3);

    return [
        'PRO_NOMBRE' => $word,
        'PRO_PREFIJO' => $prefijo,
    ];
});
