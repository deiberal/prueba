<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipTipoDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TIP_TIPO_DOC', function (Blueprint $table) {
            $table->id();
            $table->string('TIP_NOMBRE', 60);
            $table->string('TIP_PREFIJO', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TIP_TIPO_DOC');
    }
}
