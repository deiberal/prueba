<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DOC_DOCUMENTO', function (Blueprint $table) {
            $table->id();
            $table->string('DOC_NOMBRE', 60);
            $table->string('DOC_CODIGO')->unique()->nullable();
            $table->text('DOC_CONTENIDO')->nullable();
            $table->foreignId('DOC_ID_TIPO')->constrained('TIP_TIPO_DOC');
            $table->foreignId('DOC_ID_PROCESO')->constrained('PRO_PROCESO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DOC_DOCUMENTO');
    }
}
