<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->createAdminUser();
        //Make 5 TipTipoDoc
        factory(App\TipTipoDoc::class, 5)->create();
        //Make 5 ProProceso
        factory(App\ProProceso::class, 5)->create();
    }

    //Funcion para crear un usuario
    private function createAdminUser()
    {
        //Verificar si existe
        if (App\User::where('email', 'admin@correo.com')->first()) {
            return;
        }
        $user = new App\User();
        $user->name = 'admin';
        $user->email = 'admin@correo.com';
        $user->password = bcrypt('admin');
        $user->email_verified_at = now();
        $user->remember_token = Str::random(10);
        $user->name = 'admin';
        $user->last_name = 'admin';
        $user->identification = '123456789';
        $user->save();
        }

}
