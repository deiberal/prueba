<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['prefix' => 'documents'], function () {
        Route::get('/create', 'DocDocumentoController@create')->name('documentos.create');
        Route::post('/store', 'DocDocumentoController@store')->name('documentos.store');
        Route::get('/{id}/edit', 'DocDocumentoController@edit')->name('documentos.edit');
        Route::put('/{id}/update', 'DocDocumentoController@update')->name('documentos.update');
        Route::delete('/{id}/destroy', 'DocDocumentoController@destroy')->name('documentos.destroy');
    });


});
