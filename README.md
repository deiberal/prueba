# PRUEBA
## INSTALACION
- Clonar el repositorio
- Ejecutar el comando `composer install` para instalar las dependencias
- Crear una base de datos en MySQL
- Copiar archivo `.env.example` en la raiz del proyecto y renombrarlo a `.env`
- Configurar las variables de entorno en el archivo `.env`
- Ejecutar el comando `php artisan migrate` para crear las tablas en la base de datos
- Ejecutar el comando `php artisan db:seed` para crear los registros de prueba
- Ejecutar el comando `php artisan serve` para iniciar el servidor

## USUARIO
- Correo: admin@correo.com
- Contraseña: admin

