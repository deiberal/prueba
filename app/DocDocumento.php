<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocDocumento extends Model
{
    public $timestamps = false;
    protected $table = 'DOC_DOCUMENTO';
    protected $primaryKey = 'id';
    protected $fillable = [
        'DOC_NOMBRE', 'DOC_CODIGO', 'DOC_CONTENIDO', 'DOC_ID_PROCESO', 'DOC_ID_TIPO'
    ];

    //Funcion que se encarga de obtener el proceso al que pertenece el documento

    public function setDocCodigoAttribute()
    {
        //Consultar el ultimo registro con este tipo y proceso
        $ultimo = DocDocumento::where('DOC_ID_TIPO', $this->DOC_ID_TIPO)
            ->where('DOC_ID_PROCESO', $this->DOC_ID_PROCESO)->orderBy('id', 'desc')->first();
        //Si no hay registros
        $i = 1;
        if ($ultimo) {
            //Seaparar el codigo por guiones y tener el ultimo
            $codigo = explode('-', $ultimo->DOC_CODIGO);
            $i = ($codigo[2]?:0) + 1;
        }
        return $this->attributes['DOC_CODIGO'] = $this->tipo()->first()->TIP_PREFIJO . '-' . $this->proceso()->first()->PRO_PREFIJO . '-' . $i;
    }

    //Funcion que se encarga de obtener el tipo de documento al que pertenece el documento

    public function tipo()
    {
        return $this->belongsTo('App\TipTipoDoc', 'DOC_ID_TIPO');
    }

    //Funcion que se encarga de guardar el campo DOC_CODIGO con el prefijo del tipo de documento y el prefijo del proceso y el id

    public function proceso()
    {
        return $this->belongsTo('App\ProProceso', 'DOC_ID_PROCESO');
    }
}
