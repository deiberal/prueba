<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProProceso extends Model
{
    protected $table = 'PRO_PROCESO';
    protected $primaryKey = 'id';
    protected $fillable = [
        'PRO_NOMBRE', 'PRO_PREFIJO'
    ];
    public $timestamps = false;
}
