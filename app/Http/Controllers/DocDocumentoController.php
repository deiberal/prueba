<?php

namespace App\Http\Controllers;

use App\DocDocumento;
use App\Http\Requests\DocDocumentRequest;
use App\ProProceso;
use App\TipTipoDoc;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class DocDocumentoController
 * CRUD de documentos
 */
class DocDocumentoController extends Controller
{
    public function create()
    {
        return view('documents.create', [
            'tipos' => TipTipoDoc::all(),
            'procesos' => ProProceso::all()
        ]);
    }

    public function store(DocDocumentRequest $request)
    {
        //Usar la validacion con DocDocumentRequest
        try {
            $request->validated();
            $documento = new DocDocumento();
            $documento->DOC_NOMBRE = $request->DOC_NOMBRE;
            $documento->DOC_ID_TIPO = $request->DOC_TIPO;
            $documento->DOC_ID_PROCESO = $request->DOC_PROCESO;
            $documento->DOC_CONTENIDO = $request->DOC_CONTENIDO;
            $documento->setDocCodigoAttribute();
            //Guardar
            $documento->save();
            return redirect()->route('home')->with('status', 'Documento creado exitosamente');
        } catch (\Exception|ValidationException $e) {
            return redirect()->route('home')->with('status', 'Error al crear el documento. '.$e->getMessage());
        }

    }

    public function edit($id)
    {
        $documento = DocDocumento::find($id);
        return view('documents.edit', [
            'documento' => $documento,
            'tipos' => TipTipoDoc::all(),
            'procesos' => ProProceso::all()
        ]);
    }

    public function update(DocDocumentRequest $request, $id)
    {
        try {
            $request->validated();
            $documento = DocDocumento::find($id);
            $documento->DOC_NOMBRE = $request->DOC_NOMBRE;
            $documento->DOC_ID_TIPO = $request->DOC_TIPO;
            $documento->DOC_ID_PROCESO = $request->DOC_PROCESO;
            $documento->DOC_CONTENIDO = $request->DOC_CONTENIDO;
            $documento->setDocCodigoAttribute();
            //Guardar
            $documento->save();
            return redirect()->route('home')->with('status', 'Documento creado exitosamente');
        } catch (\Exception|ValidationException $e) {
            return redirect()->route('home')->with('status', 'Error al crear el documento. '.$e->getMessage());
        }
    }

    public function destroy($id)
    {
        $documento = DocDocumento::find($id);
        $documento->delete();
        return redirect()->route('home')->with('status', 'Documento eliminado exitosamente');
    }

}
