<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipTipoDoc extends Model
{
    protected $table = 'TIP_TIPO_DOC';
    protected $primaryKey = 'id';
    protected $fillable = [
        'TIP_NOMBRE', 'TIP_PREFIJO'
    ];
    public $timestamps = false;
}
